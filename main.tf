provider "aws" {
  region  = "eu-west-2" # London
  version = "~> 2.0"
}

module "simplestack" {
  source  = "app.terraform.io/willhallonline/simplestack/aws"
  version = "0.0.3"
  project_tags = {
    Name       = "Instance-Simplestack"
    Owner      = "Will Hall"
    Purpose    = "Testing"
    CostCenter = "0002"
  }
  instance_type = "t2.medium"
}